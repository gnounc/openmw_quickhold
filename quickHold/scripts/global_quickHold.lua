local types = require('openmw.types')

local function payCharge(data)
	local currentCharge = types.Item.getEnchantmentCharge(data.item)
	types.Item.setEnchantmentCharge(data.item, currentCharge - data.charge)
end

return { eventHandlers = { payCharge = payCharge } }
