local ui = require('openmw.ui')
local self = require('openmw.self')
local core = require('openmw.core')
local types = require('openmw.types')
local input = require('openmw.input')
local async = require('openmw.async')
local aux_util = require('openmw_aux.util')
local Controls = require('openmw.interfaces').Controls

local time = 0
local lasttime = 0
local lastPaid = 0
local tapTime = 0
local holdThreshold = 0.2
local doOnce = false
local quickKeys = {}
local quickSpells = {}
local savedStance = types.Actor.STANCE.Nothing	--stance to revert to when hold magic becomes inactive

local didTapCast = 0

local prevStance = nil --we have to track old stances so we know what to transition back to. 
	--(stance has already changed) by the time we arrive at onAction/onPress()

local dbg = false

--TODO!! refactor startEffects and endEffects to put them where they make sense with the new format.

--	consider keeping the current input framework, but sending out event signals for onpress, ondoublepress etc
--		the question there, is if we catch it in the same script, is that the next frame? and does that matter?

--TODO add setting to grant fortify ability from the start.
--	script currently does this, but players might not appreciate it.

--	todo: 
--		add all summoning, alteration, restoration and illusion spells.
--		calculate spell cost based on magic school cost or just do (magic cost / duration) / fps


local isQuickkey = 
{
	[input.ACTION.QuickKey1] = true,
	[input.ACTION.QuickKey2] = true,
	[input.ACTION.QuickKey3] = true,
	[input.ACTION.QuickKey4] = true,
	[input.ACTION.QuickKey5] = true,
	[input.ACTION.QuickKey6] = true,
	[input.ACTION.QuickKey7] = true,
	[input.ACTION.QuickKey8] = true,
	[input.ACTION.QuickKey9] = true,
	[input.ACTION.QuickKey10] = true
}


local function d_message(msg)
	if not dbg then return end

	ui.showMessage(msg)
end

local function d_print(fname, msg)
	if not dbg then return end

	if fname == nil then
		fname = "\x1b[35mnil"
	end

	if msg == nil then
		msg = "\x1b[35mnil"
	end

	print("\n\t\x1b[33;3m" .. fname .. "\n\t\t\x1b[33;3m" .. msg .. "\n\x1b[39m")
end


local function getMagicSound(spell, spellEvent)
	if spell == nil then return nil end

	local spellSuffixes = {area = "a", cast = "c", fail = "fail", hit = "h", bolt = "t"}
	local spellAbv = {alteration = "altr", conjuration = "conj", destruction = "dest", illusion = "illu", mysticism = "myst", restoration = "rest"}


	local spellSchool = spell.effects[1].effect.school
	
	-- sound/fx/magic/destc.wav
	local soundFile = "sound/fx/magic/" .. spellAbv[spellSchool] .. spellSuffixes[spellEvent] .. ".wav"
		d_print("getMagicSound", soundFile)
	return soundFile
end


local function startSound(soundId, object)
	core.sound.playSoundFile3d(soundId, object)	
end

local function endSound(soundId, object)
	if not core.sound.isSoundPlaying(soundId, object) then return end

	core.sound.stopSoundFile3d(soundId, object) 	
end

local function isEnchantment(spell)
	return spell.charge ~= nil
end

local function getEquippedShield()
	return types.Actor.getEquipment(self)[types.Actor.EQUIPMENT_SLOT.CarriedRight]
end

local function getCurrentStance()
	return types.Actor.getStance(self)
end

local function getEquippedWeapon()
	return types.Actor.getEquipment(self)[types.Actor.EQUIPMENT_SLOT.CarriedLeft]
end

local function getSelectedEnchantedItem()
	return types.Actor.getSelectedEnchantedItem(self)
end

local function getEnchantmentFromitem(item)
	if not item then return nil end 

	return core.magic.enchantments[item.type.record(item).enchant]
end

local function getSelectedSpell()

	quickspell = types.Actor.getSelectedSpell(self)

	if quickspell == nil then
		enchantedItem = getSelectedEnchantedItem()
		quickspell = getEnchantmentFromitem(enchantedItem)	
	end

	return quickspell

end

local function getEffectName(effect)
	local effectName = "gnounc_" .. effect.effect.id

	if effect.effect.id == "fortifyattribute" then 
		effectName = effectName .. "_" .. effect.affectedAttribute
	elseif effect.effect.id == "fortifyskill" then
		effectName = effectName .. "_" .. effect.affectedSkill
	end

	return effectName
end

--thanks zackhasacat
--https://gitlab.com/OpenMW/openmw/-/issues/7550
function getSpellByName(spellId)
	local success, result = pcall(function()
		return core.magic.spells[spellId]
	end)

	if success then
		return result
	else
		return nil
	end
end

if not types.Actor.activeEffects then
	error("OpenMW version out of date.")
end

local function getFatigue()
	return types.Actor.stats.dynamic.fatigue(self).current
end

local function getCharge(item)
	return types.Item.getEnchantmentCharge(item)
end

local function getMagicka()
	return types.Actor.stats.dynamic.magicka(self).current
end


local function getChanceToCastPenalty(spell)
	return 1.33 --TODO lazy placeholder (fatigue, school skill)
end

local function getAbilityCost(spell, isEnchantment)
	if spell == nil or spell == "" then return 0 end

	local duration = spell.effects[1].duration
	local mana = nil

	if isEnchantment then mana = spell.charge else mana = getMagicka() end

	local uses = mana / spell.cost
	local costPerSecond = mana / (uses * duration) 

	return costPerSecond * 1.5
end

--returns boolean, true if cost was payable, false if insufficient mana/charge
local function paySpellCost(quickspell)

	local pay = false
	if time > lastPaid + 2 then
		lastPaid = time
		pay = true
	end

	local skipCost = false

	local spell = quickspell.spell
	local cost = nil

	if isEnchantment(spell) then

		cost = getAbilityCost(spell, true)

		d_print("paySpellCost", "was enchantment")

		local chrg = getCharge(quickspell.item)

		d_print("paySpellCost-chrg: ", chrg)
		d_print("paySpellCost-cost: ", cost)

		if chrg > cost then 
			--limit how often we pay 
			if pay then
				core.sendGlobalEvent("payCharge", {charge = cost, item = quickspell.item})
			end
			return true
		else
			--play fail sounds
			local snd = getMagicSound(spell, "fail")
			startSound(snd, self)

			d_print("paySpellCost", "out of charge.")
			return false
		end
	else

		cost = getAbilityCost(spell, false)

		if getMagicka() > cost then
			--limit how often we pay 
			if pay then
				types.Actor.stats.dynamic.magicka(self).current = types.Actor.stats.dynamic.magicka(self).current - cost
			end
			return true
		else
			--play fail sound
			local snd = getMagicSound(spell, "fail")
			startSound(snd, self)

			d_print("paySpellCost", "out of mana.")
			return false
		end
	end
end



local function startEffects(key)

	--not quite sure where this should go
	savedStance = prevStance

	local selectedSpell = getSelectedSpell()
	local selectedItem = getSelectedEnchantedItem()

	if selectedSpell == nil then return end

	if quickSpells[tonumber(key)] == nil then
		quickSpells[tonumber(key)] = {endtime = 0, state = ""}
	end

	local quickspell = quickSpells[tonumber(key)]

	quickspell.starttime = time
	quickspell.item = selectedItem
	quickspell.spell = selectedSpell


	for _, effect in ipairs(quickspell.spell.effects) do
		d_print("startEffects", effect.effect.name .. " added")

		local effectName = getEffectName(effect)
		local ability = getSpellByName(effectName) --check to see if we've hacked in an aa_ ability for this effect.

		if ability ~= nil then
			local snd = getMagicSound(quickspell.spell, "cast")
			startSound(snd, self)

			types.Actor.spells(self):add(ability)
		end
	end
end

local function endEffects(quickspell)

	d_print("endEffects", "ending " .. quickspell.spell.effects[1].effect.name .. " effects.")

	for key, effect in ipairs(quickspell.spell.effects) do
		local effectName = getEffectName(effect)
		local ability = getSpellByName(effectName) --check to see if we've hacked in an aa_ ability for this effect.

		if ability ~= nil then
			d_print("endeffects-loop", "ending " .. effect.effect.name .. " effect.")
			types.Actor.spells(self):remove(ability)
		end
	end

	--mark it as nil so we can differentiate between first frame of hold (and starteffects), vs subsequent frame of hold (paycost)
	quickspell.state = ""
	quickspell.endtime = time
	types.Actor.setStance(self, savedStance)		--doesnt seem to work

	local snd = getMagicSound(quickspell.spell, "fail")
	startSound(snd, self)
end

--delay each step so that the spell is cast when the hands are up (doesnt work otherwise)
--	and casting is finished before we remove .use (doesnt work otherwise)
local function tapCast()
	async:newUnsavableSimulationTimer(0.1, function()  
--		savedStance = prevStance	cant seem to get this working so *shrug*
		d_print("handleTC", "putting hands up")
		types.Actor.setStance(self, types.Actor.STANCE.Spell)
	end)

	async:newUnsavableSimulationTimer(0.2, function()  
		d_print("handleTC", "firing spell")
		Controls.overrideCombatControls(true)
		self.controls.use = 1
	end)

	async:newUnsavableSimulationTimer(0.5, function()  
		d_print("handleTC", "resetting state")
		Controls.overrideCombatControls(true)
		self.controls.use = 0
	end)

	tapTime = 0
end

local function onTap(key)
	d_message("onTap key(" .. tonumber(key) .. ")")
	d_print("onTap", "key (" .. tonumber(key) .. ")")

	tapTime = time	--record that a tap happened, so that onupdate can check for a timeout, and execute it there

	local quickkey = quickKeys[tonumber(key)]
	quickkey.endtime = time
end

local function onDoubleTap(key)
--	d_message("onDoubleTap key(" .. tonumber(key) .. ")")
	
	local quickspell = quickSpells[tonumber(key)]

	--execute doubletap function here

	if quickspell == nil or quickspell.state == "" then
		startEffects(key)		--quickspell is populated here
		quickspell = quickSpells[tonumber(key)]
		quickspell.state = "toggled"
	elseif quickspell.state == "toggled" then
		endEffects(quickspell)
		quickspell.state = ""
	end

	tapTime = 0	--reset taptime so taps dont repeatedly execute
	quickKeys[tonumber(key)].starttime = 1 --hack to prevent tap from getting called again.
	d_print("onDoubleTap-end", "resetting taptime")
	d_print("onDoubleTap-tapTime", tapTime)
end

local function onPress(key)
	print("\n")
	d_print("onPress", "key (" .. tonumber(key) .. ")")

	if quickKeys[tonumber(key)] == nil then
		quickKeys[tonumber(key)] = {starttime = time, endtime = 0, state = ""}
	end

	local quickkey = quickKeys[tonumber(key)]
	quickkey.starttime = time

	--if this press is 
	local doubleTapped = (time - quickkey.starttime) < (holdThreshold * 2) and (time - quickkey.endtime) < holdThreshold
	if doubleTapped == true then
		onDoubleTap(key)
	end

	--nothing i want to do here.
	--we already cover onHold, onTap, onRelease and onDoubletap
end

local function onHold(key)
	d_print("onHold", "key (" .. tonumber(key) .. ")")

	--execute hold function here (paycost?)
	local quickspell = quickSpells[tonumber(key)]
	if quickspell == nil or quickspell.state == "" then
		startEffects(key)
		quickspell = quickSpells[tonumber(key)]
		quickspell.state = "held"
	elseif quickspell.state == "held" then
		paySpellCost(quickspell)
	end
end

local function onRelease(key)
	d_print("onRelease", "key (" .. tonumber(key) .. ")\n")

	local quickspell = quickSpells[tonumber(key)]
	local quickkey = quickKeys[tonumber(key)]

	local keyTapped = (time - quickkey.starttime) < holdThreshold

	if keyTapped == true then
		onTap(key)
		return
	end


	d_message("onRelease key(" .. tonumber(key) .. ")")
	d_print("onRelease", "key (" .. tonumber(key) .. ")\n")

	--execute release function here (not tap or doubletap.)
	--wasnt a tap, wasnt a doubletap. clear to remove effects

	endEffects(quickspell)
	quickkey.endtime = time
end

local function onInputAction(action)
	if core.isWorldPaused() or core.getSimulationTime() == 0 then
		return
	end

	if isQuickkey[action] then
		onPress(tonumber(action))
	end
end

local function onUpdate(dt)

	--add delay so a doubleclick can be registered
	if tapTime > 0 and (time - tapTime) > holdThreshold then
		d_print("onUpdate-tapTime", tapTime)
		tapCast()
	end

	if not doOnce then
		print("\x1b[35mDebug is " .. tostring(dbg))
		types.Actor.spells(self):add("gnounc_fortath")
		doOnce = true
	end    

	time = time + dt
	local stance = getCurrentStance()

	--removed things payspellcost when key is held

	for key, value in pairs(isQuickkey) do
		local quickkey = quickKeys[tonumber(key)]

		if quickkey ~= nil then
			--onPress() is handled in onInputAction(action)
			local hold = (time > (quickkey.starttime + holdThreshold)) and (quickkey.starttime > quickkey.endtime)
			local release = quickkey.starttime > quickkey.endtime and (input.isActionPressed(key) == false)
			--onDoublePress() is handled in onInputAction(action)

			--if press then onPress(key)
			if hold then onHold(key) end
			if release then onRelease(key) end
			--if doublePress then onDoublePress(key) end
		end
	end

	prevStance = stance

end


return {engineHandlers = { onInputAction = onInputAction, onUpdate = onUpdate } }

