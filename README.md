# openMW_quickHold



## Requires OpenMW 0.49 or above

## Youtube Demonstration
https://www.youtube.com/watch?v=uPX7e4GGXBE

## Installation
--todo

## To Use:
1. Bind a quickAction to a spell or enchantment  that uses a supported ability
2. Hold the quickAction button to cast, and let go to stop casting.

## Currently Supported abilities
* detect animal
* fortify athletics
* fortif speed
* levitate
* light
* nighteye
* restore health
* sanctuary
* summon dremora
* summon scamp
* telekinesis

## Known Bugs:
1. Sometimes tapcast gets stuck on. I need to spend some time getting reproduction steps to find out why.


## License
--TODO: say how it is licensed.

